/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
			function getPersonsData(){
				let fullName = prompt("Full Name");
				let age = prompt("Age");
				let location = prompt("location ")

				console.log("Hello, " +fullName);
				console.log("You are "+age+" years old.");
				console.log("You live in "+location);
			}
			getPersonsData();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
			function favoriteArtist (){
				let artist1 = "1.Moira";
				let artist2 = "2.IU";
				let artist3 = "3.Redvelvet"
				let	artist4 = "4.btob "
				let	artist5 = "5.rico blanco"

				console.log(artist1);
				console.log(artist2);
				console.log(artist3);
				console.log(artist4);
				console.log(artist5);
			}
			console.log("Top 5 Favorite Band/Artist: ")
			favoriteArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

			function favoriteMovies() {
				let movie1 = "1. Black Panther Wakanda Forever"
				let movie2 = "2. One Piece Film Red";
				let movie3 = "3. smile ";
				let movie4 = "4. Black Adam";
				let movie5 = "5. Barbarian";
			
				let	raitingMovie1 = "95%";
				let	raitingMovie2 = "95%";
				let	raitingMovie3 = "78%";
				let	raitingMovie4 = "89%";
				let	raitingMovie5 = "71%";

					console.log(movie1);
					console.log("Rotten Tomatoes Rating : " +  raitingMovie1);
					console.log(movie2);
					console.log("Rotten Tomatoes Rating : " + raitingMovie2);
					console.log(movie3);
					console.log("Rotten Tomatoes Rating : " +raitingMovie3);
					console.log(movie4);
					console.log("Rotten Tomatoes Rating : " + raitingMovie4);
					console.log(movie5);
					console.log("Rotten Tomatoes Rating : " + raitingMovie5);						
						

			};
			console.log("Top 5 Favorite Movies: ");
			favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

};
printFriends();

// console.log(friend1);
// console.log(friend2);